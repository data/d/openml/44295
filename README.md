# OpenML dataset: Meta_Album_APL_Mini

https://www.openml.org/d/44295

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

## **Meta-Album Airplanes Dataset (Mini)**
***
The original Airplanes dataset (https://zenodo.org/record/3464319) comprises more than 9 000 remote sensing images acquired from Google Earth satellite imagery, including 21 different types of aircraft from around 36 airports. All the images were carefully labeled by seven specialists in the field of remote sensing image interpretation. Each class can have 230 to 846 images, where each image contains only one complete aircraft, and they have variable resolutions. To preprocess this dataset, we cropped the images from either side to make them square, and then we resized them into 128x128 px using an anti-aliasing filter.  



### **Dataset Details**
![](https://meta-album.github.io/assets/img/samples/APL.png)

**Meta Album ID**: VCL.APL  
**Meta Album URL**: [https://meta-album.github.io/datasets/APL.html](https://meta-album.github.io/datasets/APL.html)  
**Domain ID**: VCL  
**Domain Name**: Vehicles  
**Dataset ID**: APL  
**Dataset Name**: Airplanes  
**Short Description**: Airplanes dataset with different aiplane models  
**\# Classes**: 21  
**\# Images**: 840  
**Keywords**: vehicles, airplanes  
**Data Format**: images  
**Image size**: 128x128  

**License (original data release)**: Creative Commons Attribution 4.0 International  
**License URL(original data release)**: https://zenodo.org/record/3464319
https://creativecommons.org/licenses/by/4.0/legalcode
 
**License (Meta-Album data release)**: Creative Commons Attribution 4.0 International  
**License URL (Meta-Album data release)**: [https://creativecommons.org/licenses/by/4.0/legalcode](https://creativecommons.org/licenses/by/4.0/legalcode)  

**Source**: Muti-type Aircraft of Remote Sensing Images: MTARSI  
**Source URL**: https://zenodo.org/record/3464319  
  
**Original Author**: Wu, Zhize  
**Original contact**:   

**Meta Album author**: Philip Boser  
**Created Date**: 01 March 2022  
**Contact Name**: Ihsan Ullah  
**Contact Email**: meta-album@chalearn.org  
**Contact URL**: [https://meta-album.github.io/](https://meta-album.github.io/)  



### **Cite this dataset**
```
@dataset{wu_zhize_2019_3464319,
  author       = {Wu, Zhize},
  title        = {{Muti-type Aircraft of Remote Sensing Images: MTARSI}},
  month        = may,
  year         = 2019,
  publisher    = {Zenodo},
  doi          = {10.5281/zenodo.3464319},
  url          = {https://doi.org/10.5281/zenodo.3464319}
}
```


### **Cite Meta-Album**
```
@inproceedings{meta-album-2022,
        title={Meta-Album: Multi-domain Meta-Dataset for Few-Shot Image Classification},
        author={Ullah, Ihsan and Carrion, Dustin and Escalera, Sergio and Guyon, Isabelle M and Huisman, Mike and Mohr, Felix and van Rijn, Jan N and Sun, Haozhe and Vanschoren, Joaquin and Vu, Phan Anh},
        booktitle={Thirty-sixth Conference on Neural Information Processing Systems Datasets and Benchmarks Track},
        url = {https://meta-album.github.io/},
        year = {2022}
    }
```


### **More**
For more information on the Meta-Album dataset, please see the [[NeurIPS 2022 paper]](https://meta-album.github.io/paper/Meta-Album.pdf)  
For details on the dataset preprocessing, please see the [[supplementary materials]](https://openreview.net/attachment?id=70_Wx-dON3q&name=supplementary_material)  
Supporting code can be found on our [[GitHub repo]](https://github.com/ihsaan-ullah/meta-album)  
Meta-Album on Papers with Code [[Meta-Album]](https://paperswithcode.com/dataset/meta-album)  



### **Other versions of this dataset**
[[Micro]](https://www.openml.org/d/44251)  [[Extended]](https://www.openml.org/d/44329)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44295) of an [OpenML dataset](https://www.openml.org/d/44295). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44295/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44295/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44295/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

